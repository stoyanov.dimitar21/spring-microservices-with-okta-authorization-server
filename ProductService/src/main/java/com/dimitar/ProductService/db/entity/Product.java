package com.dimitar.ProductService.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {

	@Id
	@Column(name = "product_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long productId;
	
	@Column(name = "product_name")
	private String productName;
	
	@Column
	private long price;
	
	@Column
	private long quantity;
	
	@Column(name = "quantity_status")
	private String quantityStatus;
	
}
