package com.dimitar.ProductService.db.mapper;

import org.springframework.stereotype.Component;

import com.dimitar.ProductService.db.entity.Product;
import com.dimitar.ProductService.db.model.ProductDto;

@Component
public class ProductMapper implements Mapper<Product, ProductDto> {

	public Product toEntity(ProductDto dto) {
		var entity = Product.builder()
				.productName(dto.getProductName())
				.price(dto.getPrice())
				.quantity(dto.getQuantity())
				.quantityStatus(dto.getQuantityStatus())
				.build();
		
		return entity;
	}
	
	public ProductDto toDto(Product entity) {
		var dto = ProductDto.builder()
				.productId(entity.getProductId())
				.productName(entity.getProductName())
				.price(entity.getPrice())
				.quantity(entity.getQuantity())
				.quantityStatus(entity.getQuantityStatus())
				.build();
		
		return dto;
	}
}
