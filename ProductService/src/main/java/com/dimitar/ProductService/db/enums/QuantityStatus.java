package com.dimitar.ProductService.db.enums;

public enum QuantityStatus {
	AVAILABLE,
	OUT_OF_STOCK
}
