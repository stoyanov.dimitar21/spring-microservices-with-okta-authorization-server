package com.dimitar.ProductService.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dimitar.ProductService.db.model.ProductDto;
import com.dimitar.ProductService.service.ProductService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("/api/product")
public class ProductController {

	private final ProductService productService;
	
	@GetMapping
	public ResponseEntity<List<ProductDto>> findAllProducts() {
		var all = productService.all();
        return ResponseEntity.status(HttpStatus.OK).body(all);
	}
	
	@GetMapping("/{id}")
    @PreAuthorize("hasAuthority('Admin') || hasAuthority('Customer') || hasAuthority('SCOPE_internal')")
	public ResponseEntity<ProductDto> findProductById(@PathVariable("id") Long id) {
		var product = productService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(product);
	}
	
	@PostMapping
    @PreAuthorize("hasAuthority('Admin')")
	public ResponseEntity<ProductDto> createProduct(@RequestBody ProductDto productDto) {
		var newProduct = productService.create(productDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(newProduct);
	}
	
	@PutMapping("/{id}/reduceQuantity")
	public ResponseEntity<Void> reduceQuantity(@PathVariable("id") Long id, @RequestParam Long quantity) {
		productService.reduceQuantity(id, quantity);
        return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
}
