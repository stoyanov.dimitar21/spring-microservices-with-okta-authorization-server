package com.dimitar.ProductService.service;

import java.util.List;

public interface CrudService<E, K> {

    List<E> all();

    E findById(K k);

    E create(E e);

    E update(E e, K k);

    void delete(K k);
}
