package com.dimitar.ProductService.service;

import com.dimitar.ProductService.db.model.ProductDto;

public interface ProductService extends CrudService<ProductDto, Long> {

	void reduceQuantity(long productId, long quantity);
}
