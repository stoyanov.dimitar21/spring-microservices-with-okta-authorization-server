package com.dimitar.ProductService.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dimitar.ProductService.db.entity.Product;
import com.dimitar.ProductService.db.enums.QuantityStatus;
import com.dimitar.ProductService.db.mapper.ProductMapper;
import com.dimitar.ProductService.db.model.ProductDto;
import com.dimitar.ProductService.exception.ProductException;
import com.dimitar.ProductService.exception.ResourceNotFoundException;
import com.dimitar.ProductService.repository.ProductRepository;
import com.dimitar.ProductService.service.ProductService;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {
	
	private final ProductRepository productRepository;
	private final ProductMapper productMapper;
	
	@Override
	public List<ProductDto> all() {
		log.info("Find all products");
		
		return productRepository.findAll()
				.stream()
				.map(productMapper::toDto)
				.collect(Collectors.toList());	
	}

	@Override
	public ProductDto findById(Long id) {
		var entity = findProductById(id);
		
		log.info("Product was found");
		return productMapper.toDto(entity);
	}

	@Override
	public ProductDto create(ProductDto dto) {
		if (dto.getQuantity() > 0) {
			dto.setQuantityStatus(QuantityStatus.AVAILABLE.toString());
		} else {
			dto.setQuantityStatus(QuantityStatus.OUT_OF_STOCK.toString());
		}
		var entity = productMapper.toEntity(dto);
		var saved = productRepository.save(entity);
		
		log.info("Product was created");
		return productMapper.toDto(saved);
	}

	@Override
	public ProductDto update(ProductDto dto, Long id) {
		var entity = findProductById(id);
		entity.setProductName(dto.getProductName());
		entity.setPrice(dto.getPrice());
		entity.setQuantity(dto.getQuantity());
		
		productRepository.save(entity);
		return dto;
	}

	@Override
	public void delete(Long id) {
        var entity = findProductById(id);
        productRepository.delete(entity);
	}

	@Override
	public void reduceQuantity(long productId, long quantity) {
		var product = findProductById(productId);
		
		if (product.getQuantity() < quantity) {
			throw new ProductException(HttpStatus.BAD_REQUEST, "Product does not have sufficient quantity");
		}
		
		product.setQuantity(product.getQuantity() - quantity);
		productRepository.save(product);
		log.info("Product quantity updated");
	}
	
	private Product findProductById(Long id) {
		return productRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", String.valueOf(id)));
	}

}
