package com.dimitar.ProductService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dimitar.ProductService.db.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

}
