## Documentation
    https://documenter.getpostman.com/view/11815367/2s93sacEK2

### Redis image for rate limiter
    docker run --name redis -d -p6379:6379 redis

### Zipkin image
    docker run -d -p 9411:9411 openzipkin/zipkin
