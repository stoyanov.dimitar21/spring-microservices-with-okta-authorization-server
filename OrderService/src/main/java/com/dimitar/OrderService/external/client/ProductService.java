package com.dimitar.OrderService.external.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.dimitar.OrderService.db.model.OrderDtoDetails.ProductDetail;
import com.dimitar.OrderService.exception.OrderException;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

//@CircuitBreaker(name = "external", fallbackMethod = "fallback")
@FeignClient(name = "PRODUCT-SERVICE/api/product")
public interface ProductService {

	@PutMapping("/{id}/reduceQuantity")
	public ResponseEntity<Void> reduceQuantity(@PathVariable("id") Long id, @RequestParam Long quantity);
	
	@GetMapping("/{id}")
	public ResponseEntity<ProductDetail> findProductById(@PathVariable("id") Long id);
	
//	default void fallback(Exception e) {
//		throw new OrderException(HttpStatus.SERVICE_UNAVAILABLE, "Product service is not avaiable");
//	}
}
