package com.dimitar.OrderService.external.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.dimitar.OrderService.db.model.OrderDtoDetails.TransactionDetail;
import com.dimitar.OrderService.exception.OrderException;
import com.dimitar.OrderService.external.request.PaymentRequest;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

//@CircuitBreaker(name = "external", fallbackMethod = "fallback")
@FeignClient(name = "PAYMENT-SERVICE/api/payment")
public interface PaymentService {

	@PostMapping
	public ResponseEntity<Long> createPayment(@RequestBody PaymentRequest request);
	
	@GetMapping("/{orderId}")
	public ResponseEntity<TransactionDetail> findPaymentByOrderId(@PathVariable Long orderId);
	
//	default void fallback(Exception e) {
//		throw new OrderException(HttpStatus.SERVICE_UNAVAILABLE, "Payment service is not avaiable");
//	}
}
