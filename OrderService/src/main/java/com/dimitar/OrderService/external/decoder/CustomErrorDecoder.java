package com.dimitar.OrderService.external.decoder;

import java.io.IOException;
import org.springframework.http.HttpStatus;

import com.dimitar.OrderService.exception.OrderException;
import com.fasterxml.jackson.databind.ObjectMapper;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomErrorDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String methodKey, Response response) {
		ObjectMapper objectMapper = new ObjectMapper();
		
		try {
			var data = objectMapper.readTree(response.body().asInputStream()).toPrettyString();
			return new OrderException(HttpStatus.BAD_REQUEST, data);
		} catch (IOException e) {
			throw new OrderException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
		}
	}

}
