package com.dimitar.OrderService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dimitar.OrderService.db.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

}
