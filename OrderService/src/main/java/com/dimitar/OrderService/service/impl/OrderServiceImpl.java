package com.dimitar.OrderService.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.dimitar.OrderService.db.entity.Order;
import com.dimitar.OrderService.db.mapper.OrderMapper;
import com.dimitar.OrderService.db.model.OrderDto;
import com.dimitar.OrderService.db.model.OrderDtoDetails;
import com.dimitar.OrderService.db.model.OrderRequest;
import com.dimitar.OrderService.exception.ResourceNotFoundException;
import com.dimitar.OrderService.external.client.PaymentService;
import com.dimitar.OrderService.external.client.ProductService;
import com.dimitar.OrderService.external.request.PaymentRequest;
import com.dimitar.OrderService.external.request.PaymentStatus;
import com.dimitar.OrderService.repository.OrderRepository;
import com.dimitar.OrderService.service.OrderService;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@AllArgsConstructor
@Service
@Log4j2
public class OrderServiceImpl implements OrderService {
	
	private final OrderRepository orderRepository;
	private final ProductService productService;
	private final PaymentService paymentService;
	private final OrderMapper orderMapper;
	
	@Override
	public List<OrderDto> findAll() {
		log.info("Find all orders");
		
		return orderRepository.findAll()
				.stream()
				.map(orderMapper::toDto)
				.collect(Collectors.toList());
	}

	@Override
	public OrderDtoDetails findById(long id) {
		var order = findOrderById(id);
		log.info("Order is founded for id: " + id);
		
		var product = productService.findProductById(order.getProductId()).getBody();
		var transaction = paymentService.findPaymentByOrderId(order.getId()).getBody();
		
		return orderMapper.toDtoWithDetails(order, product, transaction);
	}


	@Override
	public long createOrder(OrderRequest request) {
		log.info("Reduce quantity for product");
		productService.reduceQuantity(request.getProductId(), request.getQuantity());
		
		var entity = orderMapper.toEntityFromRequest(request);
		var order = orderRepository.save(entity);
		
		log.info("Calling payment service to complete the payment");
		PaymentRequest paymentRequest = buildPaymentRequest(order, request);
		
		String orderStatus = null;
		try {
			paymentService.createPayment(paymentRequest);
			orderStatus = PaymentStatus.SUCCESS.name();
			log.info("Payment done successfully");
		} catch (Exception e) {
			orderStatus = PaymentStatus.FAIL.name();
			log.info("Payment fail");
		}
		
		order.setOrderStatus(orderStatus);
		orderRepository.save(order);
		
		log.info("Order is created sucessufully");
		return order.getId();
	}
	
	private PaymentRequest buildPaymentRequest(Order order, OrderRequest request) {
		return PaymentRequest.builder()
				.orderId(order.getId())
				.paymentMode(request.getPaymentMode())
				.amount(request.getTotalAmount())
				.build();
	}

	private Order findOrderById(Long id) {
		return orderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Order", "id", String.valueOf(id)));
	}
	
}
