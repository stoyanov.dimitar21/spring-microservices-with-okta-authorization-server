package com.dimitar.OrderService.service;

import java.util.List;

import com.dimitar.OrderService.db.model.OrderDto;
import com.dimitar.OrderService.db.model.OrderDtoDetails;
import com.dimitar.OrderService.db.model.OrderRequest;

public interface OrderService {
	
	List<OrderDto> findAll();
	
	OrderDtoDetails findById(long id);
	
	long createOrder(OrderRequest request);

}
