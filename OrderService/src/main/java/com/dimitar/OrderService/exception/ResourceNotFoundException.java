package com.dimitar.OrderService.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
@Getter
public class ResourceNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1612286082976469866L;
	private final String resource;
    private final String name;
    private final String value;

    public ResourceNotFoundException(String resource, String name, String value) {
        super(String.format("%s was not found with %s : '%s'", resource, name, value));
        this.resource = resource;
        this.name = name;
        this.value = value;
    }

}