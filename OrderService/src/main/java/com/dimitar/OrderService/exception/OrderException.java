package com.dimitar.OrderService.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class OrderException extends RuntimeException {
	
	private static final long serialVersionUID = -528118649352586180L;
	private final HttpStatus httpStatus;
    private final String message;

    public OrderException(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public OrderException(String message, HttpStatus httpStatus, String message1) {
        super(message);
        this.httpStatus = httpStatus;
        this.message = message1;
    }

}
