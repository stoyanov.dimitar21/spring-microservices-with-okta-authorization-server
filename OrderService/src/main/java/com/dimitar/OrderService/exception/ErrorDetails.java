package com.dimitar.OrderService.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class ErrorDetails {

    private final LocalDate timestamp;
    private final String message;
    private final String details;
    
}