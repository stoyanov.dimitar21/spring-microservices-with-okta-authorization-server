package com.dimitar.OrderService.db.model;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDtoDetails {

	private OrderDto orderDto;
	private ProductDetail productDetail;
	private TransactionDetail transactionDetail;
	
	@Data
	@Builder
	@AllArgsConstructor
	@NoArgsConstructor
	public static class ProductDetail {
		
		private long productId;
		private String productName;
		private long price;
		private long quantity;
		private String quantityStatus;
	}
	
	@Data
	@Builder
	@AllArgsConstructor
	@NoArgsConstructor
	public static class TransactionDetail {

		private long id;
		private long orderId;
		private String paymentMode;
		private String referenceNumber;
		private LocalDateTime paymentDate;
		private String status;
		private long amount;
	}
}
