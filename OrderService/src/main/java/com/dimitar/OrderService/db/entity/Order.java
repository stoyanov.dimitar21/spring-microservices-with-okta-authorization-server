package com.dimitar.OrderService.db.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "order_details")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "product_id")
	private long productId;
	
	@Column
	private long quantity;
	
	@Column(name = "order_date")
	@CreationTimestamp
	private LocalDateTime orderDate;
	
	@Column(name = "order_status")
	private String orderStatus;
	
	@Column
	private long amount;
	
}
