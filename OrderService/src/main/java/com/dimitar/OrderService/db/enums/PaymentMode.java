package com.dimitar.OrderService.db.enums;

public enum PaymentMode {
	CASH,
	PAYPAL,
	DEBIT_CARD,
	CREDIT_CARD,
	APPLE_PAY
}
