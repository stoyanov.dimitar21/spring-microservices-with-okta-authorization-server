package com.dimitar.OrderService.db.model;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {

	private long id;
	private long productId;
	private long quantity;
	private LocalDateTime orderDate;
	private String orderStatus;
	private long amount;

}
