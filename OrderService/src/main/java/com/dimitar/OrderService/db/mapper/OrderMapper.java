package com.dimitar.OrderService.db.mapper;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

import com.dimitar.OrderService.db.entity.Order;
import com.dimitar.OrderService.db.enums.OrderStatus;
import com.dimitar.OrderService.db.model.OrderDto;
import com.dimitar.OrderService.db.model.OrderDtoDetails;
import com.dimitar.OrderService.db.model.OrderDtoDetails.ProductDetail;
import com.dimitar.OrderService.db.model.OrderDtoDetails.TransactionDetail;
import com.dimitar.OrderService.db.model.OrderRequest;

@Component
public class OrderMapper implements Mapper<Order, OrderDto>{

	@Override
	public Order toEntity(OrderDto dto) {
		var entity = Order.builder()
				.productId(dto.getProductId())
				.quantity(dto.getQuantity())
				.orderDate(dto.getOrderDate())
				.orderStatus(dto.getOrderStatus())
				.amount(dto.getAmount())
				.build();
		
		return entity;
	}

	@Override
	public OrderDto toDto(Order entity) {
		var dto = OrderDto.builder()
				.id(entity.getId())
				.productId(entity.getProductId())
				.quantity(entity.getQuantity())
				.orderDate(entity.getOrderDate())
				.orderStatus(entity.getOrderStatus())
				.amount(entity.getAmount())
				.build();
		
		return dto;
	}
	
	public OrderDtoDetails toDtoWithDetails(Order order, ProductDetail productDetail, TransactionDetail transactionDetail) {
		var details = OrderDtoDetails.builder()
				.orderDto(toDto(order))
				.productDetail(productDetail)
				.transactionDetail(transactionDetail)
				.build();

		return details;
	}

	public Order toEntityFromRequest(OrderRequest request) {
		var entity = Order.builder()
				.amount(request.getTotalAmount())
				.orderStatus(OrderStatus.CREATED.toString())
				.productId(request.getProductId())
				.orderDate(LocalDateTime.now())
				.quantity(request.getQuantity())
				.build();
		
		return entity;
	}

}
