package com.dimitar.OrderService.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dimitar.OrderService.db.model.OrderDto;
import com.dimitar.OrderService.db.model.OrderDtoDetails;
import com.dimitar.OrderService.db.model.OrderRequest;
import com.dimitar.OrderService.service.OrderService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("/api/order")
public class OrderController {

	private final OrderService orderService;
	
	@GetMapping
	public ResponseEntity<List<OrderDto>> findAllOrders() {
		var all = orderService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(all);
	}
	
	
	@GetMapping("/{id}")
    @PreAuthorize("hasAuthority('Admin') || hasAuthority('Customer')")
	public ResponseEntity<OrderDtoDetails> findOrderDetails(@PathVariable long id) {
		var order = orderService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(order);
	}
	
	@PostMapping
    @PreAuthorize("hasAuthority('Customer')")
	public ResponseEntity<Long> createOrder(@RequestBody OrderRequest orderRequest) {
		var id = orderService.createOrder(orderRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(id);
	}
	
	
}
