package com.dimitar.PaymentService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dimitar.PaymentService.db.entity.TransactionDetail;

@Repository
public interface PaymentRepository extends JpaRepository<TransactionDetail, Long> {

	TransactionDetail findByOrderId(long orderId);
}
