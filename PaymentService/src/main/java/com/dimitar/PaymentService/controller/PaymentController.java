package com.dimitar.PaymentService.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dimitar.PaymentService.db.model.PaymentRequest;
import com.dimitar.PaymentService.db.model.TransactionDetailDto;
import com.dimitar.PaymentService.service.PaymentService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/api/payment")
public class PaymentController {

	private final PaymentService paymentService;
	
	@GetMapping
	public ResponseEntity<List<TransactionDetailDto>> findAll() {
		var all = paymentService.findAll();
		return ResponseEntity.status(HttpStatus.OK).body(all);
	}
	
	@GetMapping("/{orderId}")
	public ResponseEntity<TransactionDetailDto> findPaymentByOrderId(@PathVariable Long orderId) {
		var payment = paymentService.findPaymentByOrderId(orderId);
		return ResponseEntity.status(HttpStatus.OK).body(payment);
	}
	
	@PostMapping
	public ResponseEntity<Long> createPayment(@RequestBody PaymentRequest request) {
		var result = paymentService.createPayment(request);
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(result);
	}
}
