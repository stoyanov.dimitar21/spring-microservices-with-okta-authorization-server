package com.dimitar.PaymentService.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.dimitar.PaymentService.db.mapper.PaymentMapper;
import com.dimitar.PaymentService.db.model.PaymentRequest;
import com.dimitar.PaymentService.db.model.TransactionDetailDto;
import com.dimitar.PaymentService.repository.PaymentRepository;
import com.dimitar.PaymentService.service.PaymentService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class PaymentServiceImpl implements PaymentService {
	
	private final PaymentRepository paymentRepository;
	private final PaymentMapper paymentMapper;
	
	@Override
	public List<TransactionDetailDto> findAll() {
		return paymentRepository.findAll()
				.stream()
				.map(paymentMapper::toDto)
				.collect(Collectors.toList());
	}
	
	@Override
	public long createPayment(PaymentRequest request) {
		var entity = paymentMapper.toEntityFromRequest(request);
		
		var saved = paymentRepository.save(entity);
		return saved.getId();
	}

	@Override
	public TransactionDetailDto findPaymentByOrderId(Long orderId) {
		var payment = paymentRepository.findByOrderId(orderId);
		return paymentMapper.toDto(payment);
	}

}
