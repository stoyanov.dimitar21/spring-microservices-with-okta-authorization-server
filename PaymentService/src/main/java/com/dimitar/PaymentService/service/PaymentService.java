package com.dimitar.PaymentService.service;

import java.util.List;

import com.dimitar.PaymentService.db.model.PaymentRequest;
import com.dimitar.PaymentService.db.model.TransactionDetailDto;

public interface PaymentService {
	
	List<TransactionDetailDto> findAll();
	
	TransactionDetailDto findPaymentByOrderId(Long id);

	long createPayment(PaymentRequest request);
}
