package com.dimitar.PaymentService.db.model;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDetailDto {

	private long id;
	private long orderId;
	private String paymentMode;
	private String referenceNumber;
	private LocalDateTime paymentDate;
	private String status;
	private long amount;
}
