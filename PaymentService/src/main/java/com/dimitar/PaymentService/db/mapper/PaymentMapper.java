package com.dimitar.PaymentService.db.mapper;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

import com.dimitar.PaymentService.db.entity.TransactionDetail;
import com.dimitar.PaymentService.db.model.PaymentRequest;
import com.dimitar.PaymentService.db.model.TransactionDetailDto;

@Component
public class PaymentMapper implements Mapper<TransactionDetail, TransactionDetailDto> {

	@Override
	public TransactionDetail toEntity(TransactionDetailDto d) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TransactionDetailDto toDto(TransactionDetail e) {
		var dto = TransactionDetailDto.builder()
				.id(e.getId())
				.amount(e.getAmount())
				.orderId(e.getOrderId())
				.paymentDate(e.getPaymentDate())
				.paymentMode(e.getPaymentMode() == null ? null : e.getPaymentMode())
				.referenceNumber(e.getReferenceNumber())
				.status(e.getStatus())
				.build();
		
		return dto;
	}
	
	public TransactionDetail toEntityFromRequest(PaymentRequest request) {
		var entity = TransactionDetail.builder()
				.amount(request.getAmount())
				.orderId(request.getOrderId())
				.referenceNumber(request.getReferenceNumber())
				.paymentMode(request.getPaymentMode() == null ? null : request.getPaymentMode().toString())
				.paymentDate(LocalDateTime.now())
				.build();
		
		return entity;
	}

}
