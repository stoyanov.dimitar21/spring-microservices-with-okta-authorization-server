package com.dimitar.PaymentService.db.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "transaction_detail")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "order_id")
	private long orderId;
	
	@Column(name = "payment_mode")
	private String paymentMode;
	
	@Column(name = "reference_number")
	private String referenceNumber;
	
	@Column(name = "payment_date")
	@CreationTimestamp
	private LocalDateTime paymentDate;
	
	@Column
	private String status;
	
	@Column
	private long amount;
}
