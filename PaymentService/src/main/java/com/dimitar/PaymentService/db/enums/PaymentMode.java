package com.dimitar.PaymentService.db.enums;

public enum PaymentMode {
	CASH,
	PAYPAL,
	DEBIT_CARD,
	CREDIT_CARD,
	APPLE_PAY
}
